import React from 'react';
import {Link} from 'react-router-dom';

import {Header, Container, Divider, Icon} from 'semantic-ui-react';
import styles from './Layout.css';
import NavigationBar from './NavigationBar';
import Footer from './Footer';


const Layout = ({ children }) => {
    return (
        <Container>
            <NavigationBar />
            {children}
            <Divider/>
            <Footer/>
        </Container>

    )
}

export default Layout;
