import React from 'react';
import { Routes , BrowserRouter as Router, Route}  from 'react-router-dom';

import Home from './Home';
import DynamicPage from './DynamicPage';
import NoMatch from './NoMatch';

import importedComponent from 'react-imported-component';
import Loading from './Loading';

const AsyncDynamicPage = importedComponent(
    () => import('./DynamicPage'),
    {
        LoadingComponent: Loading
    }
)

const AsyncNoMatch = importedComponent(
    () => import('./NoMatch'),
    {
        LoadingComponent: Loading
    }
)

const App = () => {
    return (
        <Router>
                <Routes >
                    <Route  path="/" exact element={ <Home/> } />
                    <Route exact path="/dynamic" element={ <AsyncDynamicPage/> } />
                    <Route element={ <AsyncNoMatch/>} />
                </Routes >
        </Router>
    );
};

export default App;