import React from 'react';
import {Link} from 'react-router-dom';

import Layout from './Layout';

const Home = () => {
    return (
        <Layout>
            <div className="home-content">
                <p>Hello World </p>
                <p>
                    <Link to='/dynamic'> Navigate to Dynamic Page</Link>
                </p>
                <div className="col-sm-6 ">

                <p>
                A content management system (CMS) is computer software used to manage the creation and modification of digital content (content management).[1][2][3] A CMS is typically used for enterprise content management (ECM) and web content management (WCM).
                </p>
                <p>

    ECM typically supports multiple users in a collaborative environment[4] by integrating document management, digital asset management, and record retention.[5]
    </p>
    <p>

    Alternatively, WCM is the collaborative authoring for websites and may include text and embed graphics, photos, video, audio, maps, and program code that display content and interact with the user.[6][7] ECM typically includes a WCM function. CMS is a web template to create your own website.
    </p>
                </div>
                {/* <div class="col-sm-6 ">
                    <p class="text-center-xs hidden-lg hidden-md hidden-sm">Stay Connect</p>
                    <div class="socials text-right text-center-xs">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div> */}
        </div>
        </Layout>
    );
}

export default Home;