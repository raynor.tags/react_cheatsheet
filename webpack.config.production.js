const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const path = require('path');
module.exports = {
    mode: 'production',
    // entry: './src/index.js',
    entry: {
        vendor: ['semantic-ui-react'],
        app: './src/index.js'
    },
    output: {
        filename: 'static/[name].[hash].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                // mode: "local",
                                // auto: true,
                                // exportGlobals: true,
                                // localIdentName: "[path][name]__[local]--[hash:base64:5]",
                                // localIdentContext: path.resolve(__dirname, "src"),
                                // localIdentHashSalt: "my-custom-hash",
                                // namedExport: true,
                                // modules: true,
                                exportLocalsConvention: "camelCase",
                                // importLoaders: 1,
                                // sourceMap: true
                                // exportOnlyLocals: false

                            },
                            sourceMap:true
                        }
                    },
                    {
                        loader: 'postcss-loader',
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            favicon: './public/favicon.ico',
            // filename: './index.html'
        }),
        new MiniCssExtractPlugin({
            filename: 'styles/styles.[hash].css'
        })
    ],
    optimization: {
        splitChunks: {
          cacheGroups: {
            styles: {
              name: 'styles',
              test: /\.css$/,
              chunks: 'all',
              enforce: true
            },
            vendor: {
              chunks: 'initial',
              test: 'vendor',
              name: 'vendor',
              enforce: true
            }
          }
        }
      },
};