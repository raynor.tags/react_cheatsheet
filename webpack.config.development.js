const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const port = process.env.PORT || 3003;
const path = require('path');
module.exports = {
    mode: 'development',
    // entry: './src/index.js',
    entry: {
        vendor: ['semantic-ui-react'],
        app: './src/index.js'
    },
    output: {
        filename: '[name].[hash].js'
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                // mode: "local",
                                // auto: true,
                                // exportGlobals: true,
                                // localIdentName: "[path][name]__[local]--[hash:base64:5]",
                                // localIdentContext: path.resolve(__dirname, "src"),
                                // localIdentHashSalt: "my-custom-hash",
                                // namedExport: true,
                                exportLocalsConvention: "camelCase",
                                // exportOnlyLocals: false

                            },
                            sourceMap:true
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            favicon: './public/favicon.ico',
            // filename: './index.html'
        })
    ],
    devServer: {
        host: 'localhost',
        port: port,
        historyApiFallback: true,
        open: true
    },
    optimization: {
        splitChunks: {
          cacheGroups: {
            styles: {
              name: 'styles',
              test: /\.css$/,
              chunks: 'all',
              enforce: true
            },
            vendor: {
              chunks: 'initial',
              test: 'vendor',
              name: 'vendor',
              enforce: true
            }
          }
        }
      },
};